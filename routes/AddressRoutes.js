const router = require('express').Router()
const AddressController = require('../src/controllers/AddressController')
const authMiddleware = require('../src/auth/auth')

router.post('/', authMiddleware, AddressController.store)
router.put('/:id', authMiddleware, AddressController.update)
router.get('/', authMiddleware, AddressController.index)
router.get('/:id', authMiddleware, AddressController.show)
router.delete('/:id', authMiddleware, AddressController.delete)

module.exports = router;