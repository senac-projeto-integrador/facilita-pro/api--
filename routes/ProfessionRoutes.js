const router = require('express').Router()
const ProfessionController = require('../src/controllers/ProfessionController')
const adminMiddleware = require('../src/auth/adminAuth')

router.post('/', adminMiddleware, ProfessionController.store)
router.put('/:id', adminMiddleware, ProfessionController.update)
router.get('/', ProfessionController.index)
router.get('/:id', ProfessionController.show)
router.delete('/:id', adminMiddleware, ProfessionController.delete)

module.exports = router;
