const router = require("express").Router();
const authMiddleware = require("../src/auth/auth");
const adminMiddleware = require("../src/auth/adminAuth");
const AppReviewsController = require("../src/controllers/AppReviewsController");

router.post("/", authMiddleware, AppReviewsController.store);
router.get("/", adminMiddleware, AppReviewsController.index);

module.exports = router;
