const router = require("express").Router();
const ServiceController = require("../src/controllers/ServiceController");
const adminMiddleware = require("../src/auth/adminAuth");

router.post("/", adminMiddleware, ServiceController.store);
router.put("/:id", adminMiddleware, ServiceController.update);
router.get("/", ServiceController.index);
router.get("/paranoid", ServiceController.withParanoid);
router.get("/:id", ServiceController.show);
router.get("/restore/:id", ServiceController.restore);
router.delete("/:id", adminMiddleware, ServiceController.delete);

module.exports = router;
