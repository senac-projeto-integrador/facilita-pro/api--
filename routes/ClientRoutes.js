const router = require("express").Router();
const ClientController = require("../src/controllers/ClientController");
const authMiddleware = require("../src/auth/auth");
const adminMiddleware = require("../src/auth/adminAuth");

router.post("/", ClientController.store);
router.put("/:id", authMiddleware, ClientController.update);
router.get("/", authMiddleware, ClientController.index);
router.get("/paginate", adminMiddleware, ClientController.paginate);
router.get("/:id", authMiddleware, ClientController.show);
router.delete("/:id", authMiddleware, ClientController.delete);
router.get("/restore/:id", adminMiddleware, ClientController.restore);

module.exports = router;
