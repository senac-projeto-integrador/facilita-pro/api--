const router = require("express").Router();
const SolicitationController = require("../src/controllers/SolicitationController");
const authMiddleware = require("../src/auth/auth");
const multer = require("multer");
const multerConfig = require("../src/config/multer");

router.post(
  "/",
  multer(multerConfig).single("file"),
  authMiddleware,
  SolicitationController.store
);
router.put("/:id", authMiddleware, SolicitationController.update);
router.get("/", authMiddleware, SolicitationController.index);
router.get("/:id", authMiddleware, SolicitationController.show);
router.delete("/:id", authMiddleware, SolicitationController.delete);

module.exports = router;
