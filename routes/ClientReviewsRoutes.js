const router = require('express').Router()
const ClientReviewsController = require('../src/controllers/ClientReviewsController')
const authMiddleware = require('../src/auth/auth')

router.post('/client/reviews/:id', authMiddleware, ClientReviewsController.store)
router.get('/avg/professional/:id', authMiddleware, ClientReviewsController.avg)

module.exports = router;
