const Client = require("../../src/models/Client");
const bcrypt = require("bcryptjs");

describe("Client unit tests", () => {
  test("Deve encriptar o password", async () => {
    const client = await Client.create({
      name: "Esther Saraiva",
      slug: "esther-saraiva",
      birth: "1972/11/07",
      cpf: "0257-7098",
      phone: "354423444",
      email: "EstherSaraiva_Moreira@live.com",
      password: "esther-saraiva123",
      recommendation: "facebook",
		});
		
		const compare = await bcrypt.compare(client.password, client.passwordHash)

		expect(compare).toBe(true)
  });
});
