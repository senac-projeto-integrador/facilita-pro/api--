const jwt = require("jsonwebtoken");
const { promisify } = require("util");
const Client = require("../models/Client");
const Professional = require("../models/Professional");
const Admin = require("../models/Admin");

module.exports = async (req, res, next) => {
  const authHeader = req.headers.authorization;

  if (!authHeader) {
    return res.status(401).json({ message: "Token not provided" });
  }

  const [, token] = authHeader.split(" ");

  try {
    const decoded = await promisify(jwt.verify)(token, process.env.APP_SECRET);

    const client = await Client.findOne({
      where: { cpf: decoded.cpf },
    });
    const prof = await Professional.findOne({
      where: { cpf: decoded.cpf },
    });
    const adm = await Admin.findOne({
      where: { cpf: decoded.cpf },
    });

    if (client) {
      req.auth = client;
      return next();
    }

    if (prof) {
      req.auth = prof;
      return next();
    }

    if (adm) {
      req.auth = adm;
      return next();
    }
    return res.status(401).json({ message: "User not found" });
  } catch (err) {
    return res.status(401).json({ message: "Token not provided", err });
  }
};
