const jwt = require("jsonwebtoken");
const { promisify } = require("util");
const Admin = require("../models/Admin");

module.exports = async (req, res, next) => {
  const authHeader = req.headers.authorization;

  if (!authHeader) {
    return res.status(401).json({ message: "Token not provided" });
  }

  const [, token] = authHeader.split(" ");

  try {
    const decoded = await promisify(jwt.verify)(token, process.env.APP_SECRET);

    const admin = await Admin.findOne({
      where: {
        cpf: decoded.cpf,
      },
    });

    if (admin) {
      req.auth = admin;
      return next();
    }

    return res.status(401).json({ message: "Admin not found" });
  } catch (err) {
    return res
      .status(401)
      .json({ message: "Token not provided or invalid", err });
  }
};
