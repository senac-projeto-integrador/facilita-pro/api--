const Sequelize = require("sequelize");
const Model = Sequelize.Model;
const sequelize = require("../connection");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

class Admin extends Model {}
Admin.init(
  {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      allowNull: false,
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    slug: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    birth: {
      type: Sequelize.DATE,
      allowNull: false,
    },
    cpf: {
      type: Sequelize.STRING,
      unique: true,
      allowNull: false,
    },
    phone: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    email: {
      type: Sequelize.STRING,
      unique: true,
      allowNull: false,
    },
    password: {
      type: Sequelize.VIRTUAL,
      allowNull: false,
    },
    passwordHash: {
      type: Sequelize.STRING,
    },
  },
  {
    sequelize,
    paranoid: true,
    deletedAt: "deletedAt",
    modelName: "admin",
    hooks: {
      beforeSave: async (admin) => {
        if (admin.password) {
          admin.passwordHash = await bcrypt.hash(admin.password, 8);
        }
      },
    },
  }
);

Admin.prototype.checkPassword = function (password) {
  return bcrypt.compare(password, this.passwordHash);
};

Admin.prototype.generateToken = function () {
  return jwt.sign({ cpf: this.cpf }, process.env.APP_SECRET, {
    expiresIn: "24h",
  });
};

module.exports = Admin;
