const Sequelize = require('sequelize')
const Model = Sequelize.Model
const sequelize = require('../connection')

class ClientReviews extends Model {}
ClientReviews.init({
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER
  },
  client_id: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  professional_id: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  value: {
    type: Sequelize.INTEGER,
    allowNull: false
  }
}, {
  sequelize,
  modelName: 'clientreviews'
})

module.exports = ClientReviews;