const Sequelize = require('sequelize')
const Model = Sequelize.Model
const sequelize = require('../connection')

class Address extends Model {}
Address.init({
  client_id: {
    type: Sequelize.INTEGER,
    references: {
      model: 'Clients',
      key: 'id',
    },
  },
  professional_id: {
    type: Sequelize.INTEGER,
    references: {
      model: 'Professionals',
      key: 'id',
    },
  },
  street: {
    type: Sequelize.STRING,
    allowNull: false
  },
  number: {
    type: Sequelize.STRING,
    allowNull: false
  },
  complement: {
    type: Sequelize.STRING,
    allowNull: true
  }, 
  neighborhood: {
    type: Sequelize.STRING,
    allowNull: false
  },
  city: {
    type: Sequelize.STRING,
    allowNull: false
  },
  zipcode: {
    type: Sequelize.STRING,
    allowNull: false
  },
}, {
  sequelize,
  modelName: 'address'
})

module.exports = Address;