const Sequelize = require("sequelize");
const Model = Sequelize.Model;
const sequelize = require("../connection");
const Profession = require("./Profession");

class Service extends Model {}
Service.init(
  {
    name: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true,
    },
    description: {
      type: Sequelize.STRING,
      allowNull: false,
    },
  },
  {
    sequelize,
    paranoid: true,
    deletedAt: "deletedAt",
    modelName: "service",
  }
);

Service.belongsTo(Profession, {
  foreignKey: "profession_id",
  as: "profession",
});
Profession.hasMany(Service, { foreignKey: "profession_id", as: "services" });

module.exports = Service;
