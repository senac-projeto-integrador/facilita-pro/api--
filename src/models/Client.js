const Sequelize = require("sequelize");
const Model = Sequelize.Model;
const sequelize = require("../connection");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const Address = require("../models/Address");
const ProfessionalReviews = require("../models/ProfessionalReviews");

class Client extends Model {}
Client.init(
  {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      allowNull: false,
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    slug: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    birth: {
      type: Sequelize.DATE,
      allowNull: false,
    },
    cpf: {
      type: Sequelize.STRING,
      unique: true,
      allowNull: false,
    },
    phone: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    email: {
      type: Sequelize.STRING,
      unique: true,
      allowNull: false,
    },
    password: {
      type: Sequelize.VIRTUAL,
      allowNull: false,
    },
    passwordHash: {
      type: Sequelize.STRING,
    },
    recommendation: {
      type: Sequelize.STRING,
      allowNull: false,
    },
  },
  {
    sequelize,
    paranoid: true,
    deletedAt: "deletedAt",
    modelName: "client",
    hooks: {
      beforeSave: async (client) => {
        if (client.password) {
          client.passwordHash = await bcrypt.hash(client.password, 8);
        }
      },
    },
  }
);

Client.prototype.checkPassword = function (password) {
  return bcrypt.compare(password, this.passwordHash);
};

Client.prototype.generateToken = function () {
  return jwt.sign({ cpf: this.cpf }, process.env.APP_SECRET, {
    expiresIn: "24h",
  });
};

Client.hasMany(Address, { foreignKey: "client_id", as: "addresses" });
Address.belongsTo(Client, { foreignKey: "client_id", as: "client" });

Client.hasMany(ProfessionalReviews, {
  foreignKey: "client_id",
  as: "assessments",
});
ProfessionalReviews.belongsTo(Client, {
  foreignKey: "client_id",
  as: "client",
});

module.exports = Client;
