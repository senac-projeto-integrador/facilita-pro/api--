const Sequelize = require('sequelize')
const Model = Sequelize.Model
const sequelize = require('../connection')

class ProfessionalReviews extends Model {}
ProfessionalReviews.init({
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER
  },
  professional_id: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  client_id: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  value: {
    type: Sequelize.INTEGER,
    allowNull: false
  }
}, {
  sequelize,
  modelName: 'professionalreviews'
})

module.exports = ProfessionalReviews;