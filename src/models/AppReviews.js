const Sequelize = require("sequelize");
const Model = Sequelize.Model;
const sequelize = require("../connection");

class AppReviews extends Model {}
AppReviews.init(
  {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      allowNull: false,
    },
    user_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    value: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    createdAt: {
      type: Sequelize.DATE,
      allowNull: false,
    },
    updatedAt: {
      type: Sequelize.DATE,
      allowNull: false,
    },
  },
  {
		sequelize,
		modelName: 'appReviews'
	}
);

module.exports = AppReviews;
