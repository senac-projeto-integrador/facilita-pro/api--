const Admin = require("../models/Admin");
const slugify = require("slugify");

class AdminController {
  async store(req, res) {
    try {
      const admin = req.body;
      const existingEmail = await Admin.findOne({
        where: { email: admin.email },
      });
      if (existingEmail) {
        return res
          .status(400)
          .json({ message: "Existing admin with this email" });
      }
      const existingCPF = await Admin.findOne({
        where: { cpf: admin.cpf },
      });
      if (existingCPF) {
        return res
          .status(400)
          .json({ message: "Existing admin with this cpf" });
      }
      try {
        let slug = slugify(admin.name, {
          lower: true,
        });
        const newAdmin = await Admin.create({
          name: admin.name,
          slug: slug,
          birth: admin.birth,
          cpf: admin.cpf,
          phone: admin.phone,
          email: admin.email,
          password: admin.password,
        });
        return res.status(201).send(newAdmin);
      } catch (err) {
        res.status(400).json({ message: "Missing data", err });
      }
    } catch (err) {
      res.status(500).json({ message: "Internal server error", err });
    }
  }

  async index(req, res) {
    try {
      const admins = await Admin.findAll({
        order: [["name", "ASC"]],
        include: [
          {
            all: true,
          },
        ],
      });
      if (admins) {
        return res.status(200).send(admins);
      } else {
        return res
          .status(404)
          .send({ message: "There are no records in the table admins" });
      }
    } catch (err) {
      res.status(500).json({ message: "Internal server error", err });
    }
  }

  async show(req, res) {
    const admin = await Admin.findOne({
      where: { id: req.params.id },
      include: [
        {
          all: true,
        },
      ],
    });
    if (admin) {
      return res.status(200).send(admin);
    } else {
      return res.status(404).json({ message: "Client not found" });
    }
  }

  async update(req, res) {
    try {
      const data = req.body;
      const admin = await Admin.findOne({
        where: { id: req.params.id },
      });
      if (!admin) {
        return res.status(404).json({ message: "Admin not found" });
      }
      try {
        let slug = slugify(data.name, {
          lower: true,
        });
        const updateAdmin = await admin.update({
          name: data.name,
          slug: slug,
          birth: data.birth,
          cpf: data.cpf,
          phone: data.phone,
          email: data.email,
          password: data.password,
        });
        res.status(200).send(updateAdmin);
      } catch (err) {
        res.status(400).json({ message: "Missing data", err });
      }
    } catch (err) {
      res.status(500).json({ message: "Internal server error", err });
    }
  }

  async delete(req, res) {
    try {
      const admin = await Admin.findOne({
        where: { id: req.params.id },
      });
      if (!admin) {
        return res.status(404).json({ message: "Admin not found" });
      }
      await Admin.destroy({
        where: { id: req.params.id },
      }).then(res.status(204).json({ message: "Admin deleted successfully" }));
    } catch (err) {
      res.status(500).json({ message: "Internal server error", err });
    }
  }

  async restore(req, res) {
    try {
      const admin = await Admin.findOne({
        where: { id: req.params.id },
        paranoid: false,
      });
      if (!admin) {
        return res.status(404).json({ message: "Admin not found" });
      }
      await admin.restore();
      return res.status(200).json({ message: "Restored admin" });
    } catch (err) {
      res.status(500).json({ message: "Internal server error", err });
    }
  }
}

module.exports = new AdminController();
