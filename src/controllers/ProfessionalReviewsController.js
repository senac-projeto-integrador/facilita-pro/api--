const ProfessionalReviews = require("../models/ProfessionalReviews");
const Professional = require("../models/Professional");
const Client = require("../models/Client");
const mail = require("../mail/util/mail.util");
class ProfessionalReviewsController {
  async store(req, res) {
    try {
      const client_id = req.params.id;

      const data = req.body;

      if (!client_id || !data.professional_id || !data.value) {
        return res.status(400).json({ message: "Missing data" });
      }

      const professional = await Professional.findByPk(data.professional_id);
      const client = await Client.findByPk(client_id);

      if (!professional || !client) {
        return res
          .status(404)
          .json({ message: "Professional or Client not found" });
      }

      await ProfessionalReviews.create({
        professional_id: data.professional_id,
        client_id: client_id,
        value: data.value,
      }).then((newReview) => res.status(201).send(newReview));

      try {
        const subject = `Avaliação enviada para ${client.name}`;
        const html =
          `Sua avaliação foi enviada com sucesso<br>` +
          `Nota de avaliação: ${data.value}`;

        await mail(professional.email, subject, html);
      } catch (err) {
        console.log(err);
      }
    } catch (err) {
      res.status(500).json({ message: "Internal server error", err });
    }
  }

  async avg(req, res) {
    try {
      const client_id = req.params.id;
      const sum = await ProfessionalReviews.sum("value", {
        where: { client_id: client_id },
      });
      const count = await ProfessionalReviews.count({
        where: { client_id: client_id },
      });
      let avg = parseInt(sum / count);
      return res.status(204).json({ average: avg });
    } catch (err) {
      res.status(500).json({ message: "Internal server error", err });
    }
  }
}

module.exports = new ProfessionalReviewsController();
