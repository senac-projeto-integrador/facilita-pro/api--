const router = require('express').Router()
const ClientController = require('./controllers/ClientController')
const AddressController = require('./controllers/AddressController')
const ProfessionalController = require('./controllers/ProfessionalController')
const ProfessionController = require('./controllers/ProfessionController')
const ServiceController = require('./controllers/ServiceController')
const SolicitationController = require('./controllers/SolicitationController')
const BudgetController = require('./controllers/BudgetController')
const ClientReviewsController = require('./controllers/ClientReviewsController')
const ProfessionalReviewsController = require('./controllers/ProfessionalReviewsController')

// Client routes
router.post('/clients', ClientController.store)
router.put('/clients/:id', ClientController.update)
router.get('/clients', ClientController.index)
router.get('/clients/:id', ClientController.show)
router.delete('/clients/:id', ClientController.delete)

// Professional routes
router.post('/professionals', ProfessionalController.store)
router.put('/professionals/:id', ProfessionalController.update)
router.get('/professionals', ProfessionalController.index)
router.get('/professionals/:id', ProfessionalController.show)
router.delete('/professionals/:id', ProfessionalController.delete)

// Address routes
router.post('/addresses', AddressController.store)
router.put('/addresses/:id', AddressController.update)
router.get('/addresses', AddressController.index)
router.get('/addresses/:id', AddressController.show)
router.delete('/addresses/:id', AddressController.delete)

// Profession routes
router.post('/professions', ProfessionController.store)
router.put('/professions/:id', ProfessionController.update)
router.get('/professions', ProfessionController.index)
router.get('/professions/:id', ProfessionController.show)
router.delete('/professions/:id', ProfessionController.delete)

// Service routes
router.post('/services', ServiceController.store)
router.put('/services/:id', ServiceController.update)
router.get('/services', ServiceController.index)
router.get('/services/:id', ServiceController.show)
router.delete('/services/:id', ServiceController.delete)

// Solicitation routes
router.post('/solicitations', SolicitationController.store)
router.put('/solicitations/:id', SolicitationController.update)
router.get('/solicitations', SolicitationController.index)
router.get('/solicitations/:id', SolicitationController.show)
router.delete('/solicitations/:id', SolicitationController.delete)

// Budget routes
router.post('/budgets', BudgetController.store)
router.put('/budgets/:id', BudgetController.update)
router.get('/budgets', BudgetController.index)
router.get('/budgets/:id', BudgetController.show)
router.delete('/budgets/:id', BudgetController.delete)

// ClientReviews routes
router.post('/client/reviews/:id', ClientReviewsController.store)
router.get('/avg/professional/:id', ClientReviewsController.avg)

// ProfessionalReviews routes
router.post('/professional/reviews/:id', ProfessionalReviewsController.store)
router.get('/avg/client/:id', ProfessionalReviewsController.avg)

router.get('/', (req, res) => {
    return res.status(200).send();
})

module.exports = router;