const CronJob = require("cron").CronJob;
const Solicitaton = require("../models/Solicitation");
const moment = require("moment");

module.exports = async () => {
  let date = moment().format("YYYY-MM-DD");

  const solicitations = await Solicitaton.findAll({
    where: { finallyDate: date, status: "aberto" },
  });

  console.log(solicitations);

  const cron = new CronJob(
    "* * 0 * * *",
    () => {
      solicitations.map((key) =>
        key.update({
          status: "expirado",
        })
      );
      solicitations.map((key) =>
        key.budgets.map((key) =>
          key.update({
            status: "expirado",
          })
        )
      );
    },
    null,
    true,
    "America/Sao_Paulo"
  );
  cron.start();
};
