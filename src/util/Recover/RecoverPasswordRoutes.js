const router = require("express").Router();
const RecoverController = require("./RecoverPasswordController");

router.post("/", RecoverController.recover);

module.exports = router;
