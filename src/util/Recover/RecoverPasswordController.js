const recoverPassword = require("./RecoverPassword");
const mail = require("../../mail/util/mail.util");

class RecoverPasswordController {
  async recover(req, res) {
    const email = req.body.email;

    const password = await recoverPassword(email);

    if (password) {
      try {
        const subject = "<FacilitaPRO> Recuperação de senha";
        const html = `Sua nova senha é (${password})`;
        await mail(email, subject, html);
      } catch (err) {
        console.log(err);
      }
      return res
        .status(200)
        .json({ message: "Success in recovering password" });
    } else {
      return res.status(404).json({ message: "User not found" });
    }
  }
}

module.exports = new RecoverPasswordController();
