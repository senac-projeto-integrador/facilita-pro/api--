const Professional = require("../../models/Professional");
const Admin = require("../../models/Admin");
const Client = require("../../models/Client");
const bcrypt = require("bcryptjs");
const faker = require("faker");

module.exports = async (email) => {
  const part = faker.random.number();
  const part2 = faker.random.word();
  const password = part2 + part.toString();
  console.log(password);
  const hash = await bcrypt.hash(password, 8);
  console.log(hash);

  const client = await Client.findOne({
    where: { email: email },
  });
  const professional = await Professional.findOne({
    where: { email: email },
  });
  const admin = await Admin.findOne({
    where: { email: email },
  });

  if (client) {
    await client.update({
      passwordHash: hash,
    });
    return password;
  } else if (professional) {
    await professional.update({
      passwordHash: hash,
    });
    return password;
  } else if (admin) {
    await admin.update({
      passwordHash: hash,
    });
    return password;
  }
  return null;
};
