"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    function random(min, max) {
      return min + Math.floor((max - min) * Math.random());
    }

    let amount = 100;
    let data = [];
    let date = new Date()

    while (amount--) {
      const client_id = random(1, 100);
      const professional_id = random(1, 100);
      const value = random(1, 5);
      const createdAt = date
      const updatedAt = date

      let review = {
        client_id,
        professional_id,
        value,
        createdAt,
        updatedAt
      };
      data.push(review);
    }

    return queryInterface.bulkInsert("clientreviews", data, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("clientreviews", null, {});
  },
};
