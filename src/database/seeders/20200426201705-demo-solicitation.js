"use strict";

const faker = require("faker");
faker.locale = "pt_BR";

module.exports = {
  up: (queryInterface, Sequelize) => {
    function random(min, max) {
      return min + Math.floor((max - min) * Math.random());
    }

    let data = [];
    let amount = 100;
    let date = new Date();

    while (amount--) {
      const client_id = random(1, 100);
      const service_id = random(1, 10);
      const address_id = random(1, 100);
      const initialDate = date;
      const description = "Descrição do serviço";
      const finallyDate = date;
      const status = "aberto";
      const rated = false;
      const createdAt = date;
      const updatedAt = date;

      let sol = {
        client_id,
        service_id,
        address_id,
        initialDate,
        description,
        finallyDate,
        status,
        rated,
        createdAt,
        updatedAt,
      };
      data.push(sol);
    }

    return queryInterface.bulkInsert("solicitations", data, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("solicitations", null, {});
  },
};
