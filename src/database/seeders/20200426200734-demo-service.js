"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    function random(min, max) {
      return min + Math.floor((max - min) * Math.random());
    }
    return queryInterface.bulkInsert(
      "services",
      [
        {
          profession_id: random(1, 5),
          name: "Trocar registro",
          description: "Trocar registro e canos",
          createdAt: new Date(),
          updatedAt: new Date(),
          deletedAt: 0,
        },
        {
          profession_id: random(1, 5),
          name: "Consertar pia",
          description: "Consertar pia e encanamento da caixa de gordura",
          createdAt: new Date(),
          updatedAt: new Date(),
          deletedAt: 0,
        },
        {
          profession_id: random(1, 5),
          name: "Reboco",
          description: "Rebocar paredes e teto",
          createdAt: new Date(),
          updatedAt: new Date(),
          deletedAt: 0,
        },
        {
          profession_id: random(1, 5),
          name: "Forma de lage",
          description: "Contrução de toda a forma da lage, sem concretar",
          createdAt: new Date(),
          updatedAt: new Date(),
          deletedAt: 0,
        },
        {
          profession_id: random(1, 5),
          name: "Ponto de energia residencial",
          description: "Intalação de ponto de energia residencial",
          createdAt: new Date(),
          updatedAt: new Date(),
          deletedAt: 0,
        },
        {
          profession_id: random(1, 5),
          name: "Resistência do chuveiro",
          description: "Trocar resistência do chuveiro",
          createdAt: new Date(),
          updatedAt: new Date(),
          deletedAt: 0,
        },
        {
          profession_id: random(1, 5),
          name: "Planta residencial",
          description: "Criar e assinar planta residencial",
          createdAt: new Date(),
          updatedAt: new Date(),
          deletedAt: 0,
        },
        {
          profession_id: random(1, 5),
          name: "Consulta",
          description: "Consulta empresarial de construção",
          createdAt: new Date(),
          updatedAt: new Date(),
          deletedAt: 0,
        },
        {
          profession_id: random(1, 5),
          name: "Limpeza",
          description: "Limpezas em geral",
          createdAt: new Date(),
          updatedAt: new Date(),
          deletedAt: 0,
        },
        {
          profession_id: random(1, 5),
          name: "Serviços gerais em obras",
          description:
            "Todos os tipos de serviço em obras, servente de pedreiro",
          createdAt: new Date(),
          updatedAt: new Date(),
          deletedAt: 0,
        },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("services", null, {});
  },
};
