"use strict";

const faker = require("faker");
const bcrypt = require("bcryptjs");
const slugify = require("slugify");
faker.locale = "pt_BR";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let data = [];
    let amount = 10;
    let date = new Date();

    while (amount--) {
      const name = faker.name.findName();
      const slug = slugify(name, {
        lower: true,
      });
      const birth = date;
      const cpf = faker.random.number();
      const phone = faker.phone.phoneNumber();
      const email = faker.internet.email(name);
      const passwordHash = await bcrypt.hash(slug + "123", 8);
      const createdAt = date
      const updatedAt = date
      const deletedAt = 0

      let admin = {
        name,
        slug,
        birth,
        cpf,
        phone,
        email,
        passwordHash,
        createdAt,
        updatedAt,
        deletedAt
      }
      data.push(admin)
    }

    return queryInterface.bulkInsert("admins", data, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("admins", null, {});
  },
};
