"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "professions",
      [
        {
          name: "Encanador",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Pedreiro",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Eletricista",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Engenheiro Civil",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Serviços Gerais",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('professions', null, {});
  },
};
