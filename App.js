require("dotenv").config({
  path: process.env.NODE_ENV == "test" ? ".env.test" : ".env",
});

const express = require("express");
const cors = require("cors");
const cron = require("./src/util/CronJobs");

class AppController {
  constructor() {
    this.express = express();

    this.cors();
    this.middlewares();
    this.routes();
    this.timed();
  }

  timed() {
    cron();
  }

  cors() {
    this.express.use(cors());
  }

  middlewares() {
    this.express.use(express.json());
    this.express.use(express.urlencoded({ extended: true }));
  }

  routes() {
    this.express.use("/", require("./routes/SessionRoutes"));
    this.express.use("/admins", require("./routes/AdminRoutes"));
    this.express.use("/clients", require("./routes/ClientRoutes"));
    this.express.use("/professionals", require("./routes/ProfessionalRoutes"));
    this.express.use("/budgets", require("./routes/BudgetRoutes"));
    this.express.use("/addresses", require("./routes/AddressRoutes"));
    this.express.use("/professions", require("./routes/ProfessionRoutes"));
    this.express.use("/services", require("./routes/ServiceRoutes"));
    this.express.use("/solicitations", require("./routes/SolicitationRoutes"));
    this.express.use("/", require("./routes/ClientReviewsRoutes"));
    this.express.use("/", require("./routes/ProfessionalReviewsRoutes"));
    this.express.use("/appreviews", require("./routes/AppReviewsRoutes"));
    this.express.use("/", require("./src/mail/MailRoutes"));
    this.express.use(
      "/recover",
      require("./src/util/Recover/RecoverPasswordRoutes")
    );
  }
}

module.exports = new AppController().express;
